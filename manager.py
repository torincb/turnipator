from cerberus import Validator
from yaml_schemas import user_data_schema
import copy
import datetime
import logging
import yaml

logger = logging.getLogger(__name__)

class DataManager:
    """
    Handles the storage and retrieval of user data, i.e. turnip prices and
    predictions.
    """
    def __init__(self, data_filename):
        """
        Initialises the class, with existing data in the file found at
        data_filename (a string). The validity of the file contents is checked
        before execution continues. If the file does not exist, it is created.
        """
        self.data_filename = data_filename
        self.data_file = None

        # calculate when last Sunday was (will return today, if today is
        # Sunday)
        self.today = datetime.date.today()
        days_delta = (self.today.weekday() + 1) % 7
        delta = datetime.timedelta(days=days_delta)
        self.last_sunday = self.today - delta

        new_file = False
        try:
            self.data_file = open(self.data_filename, "r")
        except FileNotFoundError:
            logger.info("Creating new file: {}".format(self.data_filename))
            new_file = True

        if new_file:
            self._create_blank_data()
        else:
            self.data = yaml.safe_load(self.data_file)
            if self.data is None:
                self._create_blank_data()

        # check data file against our very own homebrewed YAML schema
        v = Validator()
        if not v.validate(self.data, user_data_schema):
            raise RuntimeError(
                "{} is invalid. The validator caught the following "
                "problem(s):\n{}".format(
                    self.data_filename, str(v.errors)
                )
            )

        date = self.data["metadata"]["sunday_date"]

        # check if the Sunday date is actually a Sunday
        if date.weekday() != 6:
            raise RuntimeError(
                "{} is invalid. The given sunday_date is not a Sunday.".format(
                    self.data_filename
                )
            )

        # check that the date is not in the future
        if date > self.today:
            raise RuntimeError(
                "{} is invalid. The given sunday_date is in the "
                "future.".format(self.data_filename)
            )

        # check if the date is a Sunday prior to the last --  if so, backup and
        # reset (same as if the bot is running and reaches Sunday)
        if date != self.last_sunday:
            self.back_up_and_reset_data()

    def back_up_and_reset_data(self):
        """
        Backs up the current data in a new file and resets the stored data.
        """
        data_copy = copy.deepcopy(self.data)

        self.data["metadata"]["sunday_date"] = self.last_sunday
        for i in range(len(self.data["data"])):
            record = self.data["data"][i]
            record["sunday_price"] = None
            record["week_prices"] = [None] * 12
            record["last_week_pattern"] = None
            record["predictions"] = []

        print(yaml.safe_dump(data_copy, default_flow_style=True, line_break=1))
        print()
        print(yaml.safe_dump(self.data))

    def _create_blank_data(self):
        self.data = {
            "metadata": {
                "sunday_date": self.last_sunday
            },
            "data": []
        }
