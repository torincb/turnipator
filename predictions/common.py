def range_length(rng):
  return rng[1] - rng[0]


def range_intersect(rng1, rng2):
  if (rng1[0] > rng2[1] or rng1[1] < rng2[0]):
    return None
  return [max(rng1[0], rng2[0]), min(rng1[1], rng2[1])]


def range_intersect_length(rng1, rng2):
  if (rng1[0] > rng2[1] or rng1[1] < rng2[0]):
    return 0
  return range_length(range_intersect(rng1, rng2))
