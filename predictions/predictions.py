"""
See README.md in this directory for info.
"""
import csv
import functools
import math
import numpy as np

from predictions.PDF import PDF
from predictions.common import (range_length, range_intersect,
    range_intersect_length)

# The reverse-engineered code is not perfectly accurate, especially as it's not
# 32-bit ARM floating point. So, be tolerant of slightly unexpected inputs

FUDGE_FACTOR = 5

FLUCTUATING = 0
LARGE_SPIKE = 1
DECREASING  = 2
SMALL_SPIKE = 3

ALL_PATTERNS = [ FLUCTUATING, LARGE_SPIKE, DECREASING, SMALL_SPIKE ]

RATE_MULTIPLIER = 10000

IDX_PATTERN_DESC        = 0
IDX_PATTERN_NUM         = 1
IDX_PRICES              = 2
IDX_PROBABILITY         = 3
IDX_WEEK_GUARANTEED_MIN = 4
IDX_WEEK_MAX            = 5
IDX_CATEGORY_TOTAL_PROB = 6


def intceil(val):
    return math.trunc(val + 0.99999)


def min_rate_from_given_and_base(given_price, buy_price):
    return RATE_MULTIPLIER * (given_price - 0.99999) / buy_price


def max_rate_from_given_and_base(given_price, buy_price):
    return RATE_MULTIPLIER * (given_price + 0.00001) / buy_price


def rate_range_from_given_and_base(given_price, buy_price):
    return [
        min_rate_from_given_and_base(given_price, buy_price),
        max_rate_from_given_and_base(given_price, buy_price)
    ]


def get_price(rate, base_price):
    return intceil(rate * base_price / RATE_MULTIPLIER)


def multiply_generator_probability(generator, probability):
    for it in generator:
        it[IDX_PROBABILITY] *= probability
        yield it


def clamp(x, mn, mx):
    return min(max(x, mn), mx)


def generate_individual_random_price(given_prices, predicted_prices, start,
    length, rate_min, rate_max):
    """
    This corresponds to the code:
      for (int i = start; i < start + length; i++)
      {
        sellPrices[work++] =
          intceil(randfloat(rate_min / RATE_MULTIPLIER, rate_max / RATE_MULTIPLIER) * basePrice);
      }

    Would return the conditional probability given the given_prices, and modify
    the predicted_prices array.
    If the given_prices won't match, returns 0.
    """
    rate_min *= RATE_MULTIPLIER
    rate_max *= RATE_MULTIPLIER

    buy_price = given_prices[0]
    rate_range = [rate_min, rate_max]
    prob = 1

    for i in range(start, start + length):
        min_pred = get_price(rate_min, buy_price)
        max_pred = get_price(rate_max, buy_price)
        if given_prices[i] is not None:
            if (given_prices[i] < min_pred - FUDGE_FACTOR or given_prices[i] > max_pred + FUDGE_FACTOR):
                # Given price is out of predicted range, so this is the wrong pattern
                return 0

            # TODO: How to deal with probability when there's fudge factor?
            # Clamp the value to be in range now so the probability won't be
            # totally biased to fudged values.
            real_rate_range = rate_range_from_given_and_base(
                clamp(given_prices[i], min_pred, max_pred), buy_price)
            prob *= range_intersect_length(rate_range, real_rate_range) / range_length(rate_range)
            min_pred = given_prices[i]
            max_pred = given_prices[i]

        predicted_prices.append([min_pred, max_pred])
    return prob


def generate_decreasing_random_price(given_prices, predicted_prices, start,
    length, start_rate_min, start_rate_max, rate_decay_min, rate_decay_max):
    """
    This corresponds to the code:
      rate = randfloat(start_rate_min, start_rate_max);
      for (int i = start; i < start + length; i++)
      {
        sellPrices[work++] = intceil(rate * basePrice);
        rate -= randfloat(rate_decay_min, rate_decay_max);
      }
                                                                                
    Would return the conditional probability given the given_prices, and modify
    the predicted_prices array.
    If the given_prices won't match, returns 0.
    """
    start_rate_min *= RATE_MULTIPLIER
    start_rate_max *= RATE_MULTIPLIER
    rate_decay_min *= RATE_MULTIPLIER
    rate_decay_max *= RATE_MULTIPLIER

    buy_price = given_prices[0]
    rate_pdf = PDF(start_rate_min, start_rate_max)
    prob = 1

    for i in range(start, start + length):
        min_pred = get_price(rate_pdf.min_value(), buy_price)
        max_pred = get_price(rate_pdf.max_value(), buy_price)
        if given_prices[i] is not None:
            if given_prices[i] < min_pred - FUDGE_FACTOR or given_prices[i] > max_pred + FUDGE_FACTOR:
                # Given price is out of predicted range, so this is the wrong pattern
                return 0

            # TODO: How to deal with probability when there's fudge factor?
            # Clamp the value to be in range now so the probability won't be
            # totally biased to fudged values.
            real_rate_range = rate_range_from_given_and_base(
                clamp(given_prices[i], min_pred, max_pred), buy_price)
            prob *= rate_pdf.range_limit(real_rate_range)
            if prob == 0:
                return 0
            min_pred = given_prices[i]
            max_pred = given_prices[i]

        predicted_prices.append([min_pred, max_pred])

        rate_pdf.decay(rate_decay_min, rate_decay_max)
    return prob


def generate_peak_price(given_prices, predicted_prices, start, rate_min,
    rate_max):
    """
    This corresponds to the code:
      rate = randfloat(rate_min, rate_max);
      sellPrices[work++] = intceil(randfloat(rate_min, rate) * basePrice) - 1;
      sellPrices[work++] = intceil(rate * basePrice);
      sellPrices[work++] = intceil(randfloat(rate_min, rate) * basePrice) - 1;
                                                                                
    Would return the conditional probability given the given_prices, and modify
    the predicted_prices array.
    If the given_prices won't match, returns 0.
    """
    rate_min *= RATE_MULTIPLIER
    rate_max *= RATE_MULTIPLIER

    buy_price = given_prices[0]
    prob = 1
    rate_range = [rate_min, rate_max]

    # * Calculate the probability first.
    # Prob(middle_price)
    middle_price = given_prices[start + 1]
    if middle_price is not None:
        min_pred = get_price(rate_min, buy_price)
        max_pred = get_price(rate_max, buy_price)
        if middle_price < min_pred - FUDGE_FACTOR or middle_price > max_pred + FUDGE_FACTOR:
            # Given price is out of predicted range, so this is the wrong pattern
            return 0

        # TODO: How to deal with probability when there's fudge factor?
        # Clamp the value to be in range now so the probability won't be
        # totally biased to fudged values.
        real_rate_range = rate_range_from_given_and_base(
            clamp(middle_price, min_pred, max_pred), buy_price)
        prob *= range_intersect_length(rate_range, real_rate_range) / range_length(rate_range)
        if prob == 0:
            return 0

        rate_range = range_intersect(rate_range, real_rate_range)

    left_price = given_prices[start]
    right_price = given_prices[start + 2]

    # Prob(left_price | middle_price), Prob(right_price | middle_price)
    #                                                                                             
    # A = rate_range[0], B = rate_range[1], C = rate_min, X = rate, Y = randfloat(rate_min, rate)
    # rate = randfloat(A, B); sellPrices[work++] = intceil(randfloat(C, rate) * basePrice) - 1;
    #                                                                                             
    # => X->U(A,B), Y->U(C,X), Y-C->U(0,X-C), Y-C->U(0,1)*(X-C), Y-C->U(0,1)*U(A-C,B-C),
    # let Z=Y-C,  Z1=A-C, Z2=B-C, Z->U(0,1)*U(Z1,Z2)
    # Prob(Z<=t) = integral_{x=0}^{1} [min(t/x,Z2)-min(t/x,Z1)]/ (Z2-Z1)
    # let F(t, ZZ) = integral_{x=0}^{1} min(t/x, ZZ)
    #    1. if ZZ < t, then min(t/x, ZZ) = ZZ -> F(t, ZZ) = ZZ
    #    2. if ZZ >= t, then F(t, ZZ) = integral_{x=0}^{t/ZZ} ZZ + integral_{x=t/ZZ}^{1} t/x
    #                                 = t - t log(t/ZZ)
    # Prob(Z<=t) = (F(t, Z2) - F(t, Z1)) / (Z2 - Z1)
    # Prob(Y<=t) = Prob(Z>=t-C)

    for price in [left_price, right_price]:
        if price is None:
            continue
        min_pred = get_price(rate_min, buy_price) - 1
        max_pred = get_price(rate_range[1], buy_price) - 1

        if price < min_pred - FUDGE_FACTOR or price > max_pred + FUDGE_FACTOR:
            # Given price is out of predicted range, so this is the wrong
            # pattern
            return 0

        # TODO: How to deal with probability when there's fudge factor?
        # Clamp the value to be in range now so the probability won't be
        # totally biased to fudged values.
        rate2_range = rate_range_from_given_and_base(clamp(price, min_pred, max_pred)+ 1, buy_price)

        def F(t, ZZ):
            if t <= 0:
                return 0
            return ZZ if (ZZ < t) else (t - t * (math.log(t) - math.log(ZZ)))

        A, B = rate_range
        C = rate_min
        Z1 = A - C
        Z2 = B - C

        def PY(t):
            return (F(t - C, Z2) - F(t - C, Z1)) / (Z2 - Z1)

        prob *= PY(rate2_range[1]) - PY(rate2_range[0])
        if prob == 0:
            return 0

    # * Then generate the real predicted range.
    # We're doing things in different order then how we calculate probability,
    # since forward prediction is more useful here.
    #
    # Main spike 1
    min_pred = get_price(rate_min, buy_price) - 1
    max_pred = get_price(rate_max, buy_price) - 1
    if given_prices[start] is not None:
        min_pred = given_prices[start]
        max_pred = given_prices[start]
    predicted_prices.append([min_pred, max_pred])

    # Main spike 2
    min_pred = predicted_prices[start][0]
    max_pred = get_price(rate_max, buy_price)
    if given_prices[start + 1] is not None:
        min_pred = given_prices[start + 1]
        max_pred = given_prices[start + 1]
    predicted_prices.append([min_pred, max_pred])

    # Main spike 3
    min_pred = get_price(rate_min, buy_price) - 1
    max_pred = predicted_prices[start + 1][1] - 1
    if given_prices[start + 2] is not None:
        min_pred = given_prices[start + 2]
        max_pred = given_prices[start + 2]
    predicted_prices.append([min_pred, max_pred])

    return prob


def generate_pattern_0_with_lengths(given_prices, high_phase_1_len,
    dec_phase_1_len, high_phase_2_len, dec_phase_2_len, high_phase_3_len):
    """
        // PATTERN 0: high, decreasing, high, decreasing, high
        work = 2;
        // high phase 1
        for (int i = 0; i < hiPhaseLen1; i++)
        {
          sellPrices[work++] = intceil(randfloat(0.9, 1.4) * basePrice);
        }
        // decreasing phase 1
        rate = randfloat(0.8, 0.6);
        for (int i = 0; i < decPhaseLen1; i++)
        {
          sellPrices[work++] = intceil(rate * basePrice);
          rate -= 0.04;
          rate -= randfloat(0, 0.06);
        }
        // high phase 2
        for (int i = 0; i < (hiPhaseLen2and3 - hiPhaseLen3); i++)
        {
          sellPrices[work++] = intceil(randfloat(0.9, 1.4) * basePrice);
        }
        // decreasing phase 2
        rate = randfloat(0.8, 0.6);
        for (int i = 0; i < decPhaseLen2; i++)
        {
          sellPrices[work++] = intceil(rate * basePrice);
          rate -= 0.04;
          rate -= randfloat(0, 0.06);
        }
        // high phase 3
        for (int i = 0; i < hiPhaseLen3; i++)
        {
          sellPrices[work++] = intceil(randfloat(0.9, 1.4) * basePrice);
        }
    """
    buy_price = given_prices[0]
    predicted_prices = [
        [buy_price, buy_price],
        [buy_price, buy_price],
    ]
    probability = 1

    # High Phase 1
    probability *= generate_individual_random_price(
        given_prices, predicted_prices, 2, high_phase_1_len, 0.9, 1.4)
    if probability == 0:
        return

    # Dec Phase 1
    probability *= generate_decreasing_random_price(
        given_prices, predicted_prices, 2 + high_phase_1_len, dec_phase_1_len,
        0.6, 0.8, 0.04, 0.1)
    if (probability == 0):
        return

    # High Phase 2
    probability *= generate_individual_random_price(
        given_prices, predicted_prices, 2 + high_phase_1_len + dec_phase_1_len,
        high_phase_2_len, 0.9, 1.4)
    if probability == 0:
        return

    # Dec Phase 2
    probability *= generate_decreasing_random_price(
        given_prices, predicted_prices, 2 + high_phase_1_len + dec_phase_1_len
        + high_phase_2_len, dec_phase_2_len, 0.6, 0.8, 0.04, 0.1)
    if probability == 0:
        return

    # High Phase 3
    if (2 + high_phase_1_len + dec_phase_1_len + high_phase_2_len + dec_phase_2_len + high_phase_3_len != 14):
        raise RuntimeError("Phase lengths don't add up")

    prev_length = 2 + high_phase_1_len + dec_phase_1_len + high_phase_2_len + dec_phase_2_len
    probability *= generate_individual_random_price(
        given_prices, predicted_prices, prev_length, 14 - prev_length, 0.9,
        1.4)
    if probability == 0:
        return

    yield [
        "Fluctuating",
        FLUCTUATING,
        predicted_prices,
        probability,
        0., 0., 0.
    ]


def generate_pattern_0(given_prices):
    """
    decPhaseLen1 = randbool() ? 3 : 2;
    decPhaseLen2 = 5 - decPhaseLen1;
    hiPhaseLen1 = randint(0, 6);
    hiPhaseLen2and3 = 7 - hiPhaseLen1;
    hiPhaseLen3 = randint(0, hiPhaseLen2and3 - 1);
    """
    for dec_phase_1_len in range(2, 4):
        for high_phase_1_len in range(0, 7):
            for high_phase_3_len in range(0, (7 - high_phase_1_len - 1 + 1)):
                g = multiply_generator_probability(
                    generate_pattern_0_with_lengths(
                        given_prices, high_phase_1_len, dec_phase_1_len, 7 -
                        high_phase_1_len - high_phase_3_len, 5 -
                        dec_phase_1_len, high_phase_3_len),
                    1 / (4 - 2) / 7 / (7 - high_phase_1_len))
                for it in g:
                    yield it


def generate_pattern_1_with_peak(given_prices, peak_start):
    """
    // PATTERN 1: decreasing middle, high spike, random low
    peakStart = randint(3, 9);
    rate = randfloat(0.9, 0.85);
    for (work = 2; work < peakStart; work++)
    {
      sellPrices[work] = intceil(rate * basePrice);
      rate -= 0.03;
      rate -= randfloat(0, 0.02);
    }
    sellPrices[work++] = intceil(randfloat(0.9, 1.4) * basePrice);
    sellPrices[work++] = intceil(randfloat(1.4, 2.0) * basePrice);
    sellPrices[work++] = intceil(randfloat(2.0, 6.0) * basePrice);
    sellPrices[work++] = intceil(randfloat(1.4, 2.0) * basePrice);
    sellPrices[work++] = intceil(randfloat(0.9, 1.4) * basePrice);
    for (; work < 14; work++)
    {
      sellPrices[work] = intceil(randfloat(0.4, 0.9) * basePrice);
    }
    """

    buy_price = given_prices[0]
    predicted_prices = [
        [buy_price, buy_price],
        [buy_price, buy_price],
    ]
    probability = 1

    probability *= generate_decreasing_random_price(
        given_prices, predicted_prices, 2, peak_start - 2, 0.85, 0.9, 0.03,
        0.05)
    if probability == 0:
        return

    # Now each day is independent of next
    min_randoms = [0.9, 1.4, 2.0, 1.4, 0.9, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4]
    max_randoms = [1.4, 2.0, 6.0, 2.0, 1.4, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9]
    for i in range(peak_start, 14):
        probability *= generate_individual_random_price(
            given_prices, predicted_prices, i, 1, min_randoms[i - peak_start],
            max_randoms[i - peak_start])
        if probability == 0:
            return
    yield [
        "Large Spike",
        LARGE_SPIKE,
        predicted_prices,
        probability,
        0., 0., 0.
    ]


def generate_pattern_1(given_prices):
    for peak_start in range(3, 10):
        g = multiply_generator_probability(
            generate_pattern_1_with_peak(given_prices, peak_start),
            1 / (10 - 3))
        for it in g:
            yield it


def generate_pattern_2(given_prices):
    """
    // PATTERN 2: consistently decreasing
    rate = 0.9;
    rate -= randfloat(0, 0.05);
    for (work = 2; work < 14; work++)
    {
      sellPrices[work] = intceil(rate * basePrice);
      rate -= 0.03;
      rate -= randfloat(0, 0.02);
    }
    break;
    """
    buy_price = given_prices[0]
    predicted_prices = [
        [buy_price, buy_price],
        [buy_price, buy_price],
    ]
    probability = 1

    probability *= generate_decreasing_random_price(
        given_prices, predicted_prices, 2, 14 - 2, 0.85, 0.9, 0.03, 0.05)
    if probability == 0:
        return

    yield [
        "Decreasing",
        DECREASING,
        predicted_prices,
        probability,
        0., 0., 0.
    ]


def generate_pattern_3_with_peak(given_prices, peak_start):
    """
    // PATTERN 3: decreasing, spike, decreasing
    peakStart = randint(2, 9);
    // decreasing phase before the peak
    rate = randfloat(0.9, 0.4);
    for (work = 2; work < peakStart; work++)
    {
      sellPrices[work] = intceil(rate * basePrice);
      rate -= 0.03;
      rate -= randfloat(0, 0.02);
    }
    sellPrices[work++] = intceil(randfloat(0.9, 1.4) * (float)basePrice);
    sellPrices[work++] = intceil(randfloat(0.9, 1.4) * basePrice);
    rate = randfloat(1.4, 2.0);
    sellPrices[work++] = intceil(randfloat(1.4, rate) * basePrice) - 1;
    sellPrices[work++] = intceil(rate * basePrice);
    sellPrices[work++] = intceil(randfloat(1.4, rate) * basePrice) - 1;
    // decreasing phase after the peak
    if (work < 14)
    {
      rate = randfloat(0.9, 0.4);
      for (; work < 14; work++)
      {
        sellPrices[work] = intceil(rate * basePrice);
        rate -= 0.03;
        rate -= randfloat(0, 0.02);
      }
    }
    """
    buy_price = given_prices[0]
    predicted_prices = [
        [buy_price, buy_price],
        [buy_price, buy_price],
    ]
    probability = 1

    probability *= generate_decreasing_random_price(
        given_prices, predicted_prices, 2, peak_start - 2, 0.4, 0.9, 0.03,
        0.05)
    if probability == 0:
        return

    # The peak
    probability *= generate_individual_random_price(
        given_prices, predicted_prices, peak_start, 2, 0.9, 1.4)
    if probability == 0:
        return

    probability *= generate_peak_price(
        given_prices, predicted_prices, peak_start + 2, 1.4, 2.0)
    if probability == 0:
        return

    if peak_start + 5 < 14:
        probability *= generate_decreasing_random_price(
            given_prices, predicted_prices, peak_start + 5,
            14 - (peak_start + 5), 0.4, 0.9, 0.03, 0.05)
        if probability == 0:
            return

    yield [
        "Small Spike",
        SMALL_SPIKE,
        predicted_prices,
        probability,
        0., 0., 0.
    ]


def generate_pattern_3(given_prices):
    for peak_start in range(2, 10):
        g = multiply_generator_probability(generate_pattern_3_with_peak(
            given_prices, peak_start), 1 / (10 - 2))
        for it in g:
            yield it


def get_transition_probability(previous_pattern):
    if previous_pattern not in ALL_PATTERNS:
        # TODO: Fill the steady state pattern
        # (https://github.com/mikebryant/ac-nh-turnip-prices/pull/90) here.
        return [ 0.346278, 0.247363, 0.147607, 0.258752 ]

    matrix = [
        [ 0.20, 0.30, 0.15, 0.35 ],
        [ 0.50, 0.05, 0.20, 0.25 ],
        [ 0.25, 0.45, 0.05, 0.25 ],
        [ 0.45, 0.25, 0.15, 0.15 ]
    ]
    return matrix[previous_pattern]


def generate_all_patterns(sell_prices, previous_pattern):
    generate_pattern_fns = [
        generate_pattern_0,
        generate_pattern_1,
        generate_pattern_2,
        generate_pattern_3
    ]
    transition_probability = get_transition_probability(previous_pattern)

    for i in range(0, 4):
        g = multiply_generator_probability(
            generate_pattern_fns[i](sell_prices), transition_probability[i])
        for it in g:
            yield it


def generate_possibilities(sell_prices, first_buy, previous_pattern):
    if first_buy or (sell_prices[0] is None):
        for buy_price in range(90, 111):
            sell_prices[0] = sell_prices[1] = buy_price
            if first_buy:
                g = generate_pattern_3(sell_prices)
                for it in g:
                    yield it
            else:
                # All buy prices are equal probability and we're at the outmost
                # layer, so don't need to multiply_generator_probability here.
                g = generate_all_patterns(sell_prices, previous_pattern)
                for it in g:
                    yield it
    else:
        g = generate_all_patterns(sell_prices, previous_pattern)
        for it in g:
            yield it


def analyze_possibilities(sell_prices, first_buy, previous_pattern):
    generated_possibilities = np.array(
        list(generate_possibilities(sell_prices, first_buy, previous_pattern)))

    total_probability = generated_possibilities[:,IDX_PROBABILITY].sum()
    generated_possibilities[:,IDX_PROBABILITY] /= total_probability

    for poss in generated_possibilities:
        weekMins = []
        weekMaxes = []
        for day in poss[IDX_PRICES][2:]:
            # Check for a future date by checking for a range of prices
            if(day[0] != day[1]):
                weekMins.append(day[0])
                weekMaxes.append(day[1])
            else:
                # If we find a set price after one or more ranged prices, the
                # user has missed a day. Discard that data and start again.
                weekMins = []
                weekMaxes = []
        if (not len(weekMins) and not len(weekMaxes)):
            weekMins.append(poss[IDX_PRICES][len(poss[IDX_PRICES]) - 1][0])
            weekMaxes.append(poss[IDX_PRICES][len(poss[IDX_PRICES]) - 1][1])
        poss[IDX_WEEK_GUARANTEED_MIN] = max(weekMins)
        poss[IDX_WEEK_MAX] = max(weekMaxes)

    category_totals = []
    for i in [0, 1, 2, 3]:
        category_totals.append(generated_possibilities[
            generated_possibilities[:,IDX_PATTERN_NUM] == i
        ][:,IDX_PROBABILITY].sum())

    for poss in generated_possibilities:
        poss[IDX_CATEGORY_TOTAL_PROB] = category_totals[poss[IDX_PATTERN_NUM]]

    # Descending sort in category probability, then individual probability
    generated_possibilities = np.flip(generated_possibilities[np.lexsort((
        generated_possibilities[:,IDX_PROBABILITY],
        generated_possibilities[:,IDX_CATEGORY_TOTAL_PROB]
    ))], 0)

    global_min_max = []
    for day in range(0, 14):
        prices = [999, 0]
        for poss in generated_possibilities:
            if (poss[IDX_PRICES][day][0] < prices[0]):
                prices[0] = poss[IDX_PRICES][day][0]
            if (poss[IDX_PRICES][day][1] > prices[1]):
                prices[1] = poss[IDX_PRICES][day][1]
        global_min_max.append(prices)

    # Put the global ('all patterns') max/mins, etc. at the start of the list
    global_stats = [
        "All patterns",
        4,
        global_min_max,
        1.0,
        generated_possibilities[:,IDX_WEEK_GUARANTEED_MIN].min(),
        generated_possibilities[:,IDX_WEEK_MAX].max(),
        1.0,
    ]
    generated_possibilities = np.insert(
        generated_possibilities, 0, global_stats, 0)

    return generated_possibilities


if __name__ == "__main__":
    prices = [
        107,  None,
        96,   92,
        89,   None,
        None, None,
        None, None,
        None, None,
        None, None,
    ]
    first_buy = False
    previous_pattern = 1

    possibilities = analyze_possibilities(prices, first_buy, previous_pattern)

    rows = [[
        "Pattern", "Prob.", "Sunday", "Mon AM", "", "Mon PM", "", "Tue AM", "",
        "Tue PM", "", "Wed AM", "", "Wed PM", "", "Thu AM", "", "Thu PM", "",
        "Fri AM", "", "Fri PM", "", "Sat AM", "", "Sat PM", "",
        "Guaranteed min.", "Potential max."
    ]]
    current_pattern_num = -1
    for poss in possibilities:

        if current_pattern_num != poss[IDX_PATTERN_NUM]:
            current_pattern_num = poss[IDX_PATTERN_NUM]
            rows.append([
                poss[IDX_PATTERN_DESC],
                "{:.2f}%".format(poss[IDX_CATEGORY_TOTAL_PROB] * 100)
            ])

        # print(poss[IDX_PRICES])
        prices = [price for day in poss[IDX_PRICES] for price in day][3:]
        rows.append([
            "",
            "{:.2f}%".format(poss[IDX_PROBABILITY] * 100),
            *prices,
            poss[IDX_WEEK_GUARANTEED_MIN],
            poss[IDX_WEEK_MAX]
        ])

    with open("predictions.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerows(rows)

