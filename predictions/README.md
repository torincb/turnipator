***Work in Progress***

This is a port of Mike Bryant's reverse-engineered turnip price predictor
(originally in JavaScript), found at:

  https://github.com/mikebryant/ac-nh-turnip-prices

Given that it is an almost direct port (done quickly), it is not very
efficient, nor very Pythonic. It does, however, apparently do the job!
