import math
import numpy as np

from predictions.common import range_length, range_intersect_length


class PDF:
    """
    Probability Density Function of rates.
    Since the PDF is continuous*, we approximate it by a discrete probability
    function:
      the value in range [(x - 0.5), (x + 0.5)) has a uniform probability
      prob[x - value_start]

    Note that we operate all rate on the (* RATE_MULTIPLIER) scale.

    (*): Well not really since it only takes values that "float" can represent
    in some form, but the space is too large to compute directly in JS.
    """

    def __init__(self, a, b, uniform = True):
        """
        Initialize a PDF in range [a, b], a and b can be non-integer.
        if uniform is true, then initialize the probability to be uniform, else
        initialize to a all-zero (invalid) PDF.
        """
        self.value_start = round(a)
        self.value_end = round(b)
        rng = [a, b]
        total_length = range_length(rng)
        self.prob = np.zeros(self.value_end - self.value_start + 1)
        if (uniform):
            for i in range(self.prob.size):
                self.prob[i] = range_intersect_length(self.range_of(i), rng) / total_length

    def range_of(self, idx):
        # TODO: consider doing the "exclusive end" properly.
        return [self.value_start + idx - 0.5, self.value_start + idx + 0.5 - 1e-9]

    def min_value(self):
        return self.value_start - 0.5

    def max_value(self):
        return self.value_end + 0.5 - 1e-9

    def normalize(self):
        self.prob /= self.prob.sum()

    def range_limit(self, rng):
        """
        Limit the values to be in the range, and return the probability that
        the value was in this range.
        """
        start, end = rng
        start = max(start, self.min_value())
        end = min(end, self.max_value())
        if (start >= end):
            # Set this to invalid values
            self.value_start = self.value_end = 0
            self.prob = []
            return 0

        prob = 0
        start_idx = round(start) - self.value_start
        end_idx = round(end) - self.value_start
        for i in range(start_idx, end_idx+1):
            bucket_prob = self.prob[i] * range_intersect_length(self.range_of(i), rng)
            self.prob[i] = bucket_prob
            prob += bucket_prob

        self.prob = self.prob[start_idx:end_idx+1]
        self.value_start = round(start)
        self.value_end = round(end)
        self.normalize()

        return prob

    def decay(self, rate_decay_min, rate_decay_max):
        """
        Subtract the PDF by a uniform distribution in [rate_decay_min,
        rate_decay_max]

        For simplicity, we assume that rate_decay_min and rate_decay_max
        are both integers.
        """
        ret = PDF(self.min_value() - rate_decay_max, self.max_value() - rate_decay_min, False)

        # O(n^2) naive algorithm for reference, which would be too slow.
        # for (let i = this.value_start i <= this.value_end i++) {
        #     const unit_prob = this.prob[i - this.value_start] / (rate_decay_max - rate_decay_min) / 2
        #     for (let j = rate_decay_min j < rate_decay_max j++) {
        #         // ([i - 0.5, i + 0.5] uniform) - ([j, j + 1] uniform)
        #         // -> [i - j - 1.5, i + 0.5 - j] with a triangular PDF
        #         // -> approximate by
        #         //        [i - j - 1.5, i - j - 0.5] uniform &
        #         //        [i - j - 0.5, i - j + 0.5] uniform
        #         ret.prob[i - j - 1 - ret.value_start] += unit_prob // Part A
        #         ret.prob[i - j - ret.value_start] += unit_prob // Part B
        #     }
        # }

        # Transform to "CDF"
        for i in range(1, self.prob.size):
            self.prob[i] += self.prob[i - 1]

        # Return this.prob[l - this.value_start] + ... + this.prob[r - 1 - this.value_start]
        # This assume that this.prob is already transformed to "CDF".
        def prob_sum(l, r):
            l -= self.value_start
            r -= self.value_start
            if (l < 0): l = 0
            if (r > self.prob.size): r = self.prob.size
            if (l >= r): return 0

            l = int(l)
            r = int(r)
            return self.prob[r - 1] - (0 if l == 0 else self.prob[l - 1])

        for x in range(0, ret.prob.size):
            # i - j - 1 - ret.value_start == x    (Part A)
            # -> i = x + j + 1 + ret.value_start, j in [rate_decay_min, rate_decay_max)
            ret.prob[x] = prob_sum(x + rate_decay_min + 1 + ret.value_start, x + rate_decay_max + 1 + ret.value_start)

            # i - j - ret.value_start == x    (Part B)
            # -> i = x + j + ret.value_start, j in [rate_decay_min, rate_decay_max)
            ret.prob[x] += prob_sum(x + rate_decay_min + ret.value_start, x + rate_decay_max + ret.value_start)

        self.prob = ret.prob
        self.value_start = ret.value_start
        self.value_end = ret.value_end
        self.normalize()

