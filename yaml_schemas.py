"""
The yaml_schema dict in this file defines the validation schema for a YAML user
data file to be processed by the DataManager class.
"""

_pattern_schema = {
    "type": "string",
    "nullable": True,
    "allowed": [
        "large spike",
        "small spike",
        "decreasing",
        "fluctuating"
    ]
}

_price_pair_schema = {
    "type": "list",
    "minlength": 2,
    "maxlength": 2,
    "schema": {
        "type": "integer",
        "nullable": True,
    }
}

_predictions_schema = {
    "type": "dict",
    "require_all": True,
    "schema": {
        "pattern": _pattern_schema,
        "sunday_price": _price_pair_schema,
        "week_prices": {
            "type": "list",
            "minlength": 12,
            "maxlength": 12,
            "schema": _price_pair_schema
        },
        "probability": {
            "type": "float",
            "nullable": True
        },
        "guaranteed_min": {
            "type": "integer",
            "nullable": True
        },
        "possible_max": {
            "type": "integer",
            "nullable": True
        },
        "category_probability": {
            "type": "float",
            "nullable": True
        }
    }
}

user_data_schema = {
    "metadata": {
        "type": "dict",
        "required": True,
        "require_all": True,
        "schema": {
            "sunday_date": {
                "type": "date"
            }
        }
    },
    "data": {
        "type": "list",
        "required": True,
        "schema": {
            "type": "dict",
            "require_all": True,
            "schema": {
                "username": {
                    "type": "string"
                },
                "sunday_price": {
                    "type": "integer",
                    "nullable": True
                },
                "week_prices": {
                    "type": "list",
                    "minlength": 12,
                    "maxlength": 12,
                    "schema": {
                        "type": "integer",
                        "nullable": True
                    }
                },
                "last_week_pattern": _pattern_schema,
                "predictions": {
                    "type": "list",
                    "schema": _predictions_schema
                }
            }
        }
    }
}
