import logging

def init_logging():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    strm_handler = logging.StreamHandler()
    strm_handler.setLevel(logging.DEBUG)
    file_handler = logging.FileHandler(filename="turnipator.log")
    file_handler.setLevel(logging.DEBUG)

    strm_formatter = logging.Formatter(
        "[%(levelname)s] %(message)s",
        datefmt="%H:%M:%S"
    )
    file_formatter = logging.Formatter(
        "%(asctime)s (%(name)s) [%(levelname)s] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S"
    )

    strm_handler.setFormatter(strm_formatter)
    file_handler.setFormatter(file_formatter)

    logger.addHandler(strm_handler)
    logger.addHandler(file_handler)
